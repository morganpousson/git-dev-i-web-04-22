**Bonjour**

On essaye de donner du _style_ à notre readme.md.

```Voici un bloc de code, pour le faire on utilise trois fois "`" ```

Pour m'aider à mettre en forme ce readme, j'ai utilisé [La documentation de Framasoft](https://docs.framasoft.org/fr/grav/markdown.html)

Essayons d'insérer une image **_maintenant_**

![Pouce vers le haut](https://images.emojiterra.com/google/android-marshmallow/128px/1f44d.png)

@Morgan85 on Gitlab

et aussi @matt.ottenwelter !